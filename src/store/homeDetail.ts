import { createSlice } from "@reduxjs/toolkit";

const initialState = {};

export const homeDetailSlice = createSlice({
  name: "homeDetail",
  initialState,
  reducers: {
    homeDetailupdate: (state, action) => {
      return action.payload;
    },
  },
});

export const { homeDetailupdate } = homeDetailSlice.actions;

export default homeDetailSlice.reducer;
