import { configureStore } from "@reduxjs/toolkit";
import homeDetailReducer from "./homeDetail";
export const store = configureStore({
  reducer: {
    profile: homeDetailReducer,
  },
});
