export const BASE_URL = "https://mocki.io/v1";
export const OPENIGLOO_LOGO =
  "https://www.openigloo.com/_next/image?url=https%3A%2F%2Fstatic.openigloo.com%2Fimages-web%2Flogo.png&w=3840&q=75";

export const footerText = {
  copyRight: "© 2023, openigloo Inc. All Rights Reserved.",
  version: "Version 2.5.2",
};

export const API = {
  SimilarListings: "/2e04a1fd-17d9-4b12-922c-a567ea62f30a",
  detail: "/73db5c35-1a5f-4eef-a7ec-89496afb87a1",
};
