import axiosInstance from "../utils/axiosInstance";

export namespace similarListingData {
  export type Response = Root[];

  export interface Root {
    imgUrl: string;
    Landmark: string;
    Street: string;
    Locality: string;
    bathroomCount: number;
    bedroomCount: number;
    listBy: string;
    price: number;
    daysPosted: number;
  }
}

export const getSimilarListingData = async (url: string) => {
  try {
    const similarListingData: similarListingData.Response =
      await axiosInstance.get(url);
    return similarListingData;
  } catch (error) {
    console.log(error);
  }
};
