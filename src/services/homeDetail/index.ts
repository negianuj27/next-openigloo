import axiosInstance from "../utils/axiosInstance";
import { homeDetailupdate } from "myApp/store/homeDetail";

export namespace homeDetailData {
  export type Response = Root;

  export interface Root {
    data: {
      imgPrimary: string;
      Landmark: string;
      Street: string;
      Locality: string;
      bathroomCount: number;
      bedroomCount: number;
      listBy: string;
      price: number;
      daysPosted: number;
      openViolations: number;
      bedbug: boolean;
      listingDate: string;
      verfied: boolean;
    };
  }
}

export const getHomeDetail = async (url: string) => {
  try {
    const homeDetailData: homeDetailData.Response = await axiosInstance.get(
      url
    );
    homeDetailupdate(homeDetailData.data);
    return homeDetailData.data;
  } catch (error) {
    console.log(error);
  }
};
