import React from "react";
import useDeviceSize from "../../../hooks/DeviceSize";
import FooterMain from "./FooterMain";
import FooterResponsive from "./FooterResponsive";

type Props = {};

const Footer = (props: Props) => {
  const [width] = useDeviceSize();
  return <>{width > 1024 ? <FooterMain /> : <FooterResponsive />}</>;
};

export default Footer;
