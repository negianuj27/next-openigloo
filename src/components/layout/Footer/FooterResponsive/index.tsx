import React from "react";
import { BottomIcons } from "../../../../config/mockData/socialIcons";

type Props = {};

const FooterResponsive = (props: Props) => {
  return (
    <div className="sticky bottom-0 w-full z-50 bg-footerBg flex justify-around py-4 items-start text-gray-400">
      {BottomIcons.map((img, i) => {
        return (
          <div
            className="flex flex-col justify-center items-center w-1/5 hover:cursor-pointer"
            key={i}
          >
            <div dangerouslySetInnerHTML={{ __html: img?.item }} />
            <p className="text-xs">{img?.name}</p>
          </div>
        );
      })}
    </div>
  );
};

export default FooterResponsive;
