import React from "react";
import InfoList from "./InfoList";
import StoreLink from "./StoreLink";
import { FooterItem } from "../../../../config/mockData/footerMock";
import { SocialIcons } from "../../../../config/mockData/socialIcons";
import { OPENIGLOO_LOGO, footerText } from "../../../../services/utils/defines";

type Props = {};

const FooterMain = (props: Props) => {
  return (
    <main>
      <div className="bg-footerBg flex justify-around  items-start p-4">
        <div className="flex flex-col justify-between items-center text-gray-400">
          <div className="h-full w-28">
            <img src={OPENIGLOO_LOGO} className="max-w-full" />
          </div>
          <div className="my-2 text-base  space-x-2 sm:text-right flex justify-around items-center">
            {SocialIcons.map((img, i) => {
              return (
                <a
                  className="text-blue-gray hover:opacity-80 cursor-pointer text-center "
                  key={i}
                >
                  <div dangerouslySetInnerHTML={{ __html: img?.item }} />
                </a>
              );
            })}
          </div>
          <p className="my-2 text-xs text-gray-500 sm:text-right ">
            {footerText.copyRight}
          </p>
          <p className="pl-3 text-xs text-gray-500">{footerText.version}</p>
        </div>
        {FooterItem.map((item) => {
          return <InfoList item={item} key={item.heading} />;
        })}
      </div>
      <StoreLink />
    </main>
  );
};

export default FooterMain;
