import React from "react";

type Props = {
  item: {
    heading: String;
    subItems: String[];
  };
};

const InfoList = ({ item }: Props) => {
  return (
    <div className="flex flex-col justify-between items-start">
      <h1 className="text-lg font-semibold">{item?.heading}</h1>
      {item?.subItems.map((sub, index) => {
        return (
          <p className="text-sm mt-2" key={index}>
            {sub}
          </p>
        );
      })}
    </div>
  );
};

export default InfoList;
