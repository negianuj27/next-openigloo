import Image from "next/image";
import React from "react";

type Props = {};

const StoreLink = (props: Props) => {
  return (
    <div className="bg-footerBg flex justify-end p-4 items-center">
      <a className="h-auto w-24">
        <Image
          src={require("../../../../assets/images/appStore.svg")}
          alt="Playstore-openIgloo"
        />
      </a>
      <a className="h-auto w-24 ml-3">
        <Image
          src={require("../../../../assets/images/playstore.svg")}
          alt="Playstore-openIgloo"
        />
      </a>
    </div>
  );
};

export default StoreLink;
