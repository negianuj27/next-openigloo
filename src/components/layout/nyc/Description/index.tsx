import React, { useMemo, useState } from "react";
import { FiChevronUp, FiChevronDown } from "react-icons/fi";

type Props = {};

export const Description = (props: Props) => {
  const [showMore, setShowMore] = useState(false);
  const [style, setStyle] = useState("");

  useMemo(() => {
    showMore
      ? setStyle("no-lineclamp whitespace-pre-line text-base text-descCol")
      : setStyle("line-clamp-6  whitespace-pre-line text-base text-descCol");
  }, [showMore]);

  const show = () => setShowMore(!showMore);

  return (
    <div className="flex flex-col justify-center items-start px-2 sm:px-0 my-10">
      <h1 className="text-xl pb-4 font-semibold text-headingCol">
        Description:
      </h1>
      <div className={style}>
        <p>
          The apartment is part of a beautiful and spacious garden style complex
          in Staten Island.
        </p>
        <p>
          <br />
        </p>
        <p>-This NEWLY renovated 1 bedroom apartment features:</p>
        <p>
          <br />
        </p>
        <p>
          -Designed kitchen with Stainless steel appliances, gas burning range
          and ample cabinet storage
        </p>
        <p>--Large windows with tons of natural light</p>
        <p>-Tiled Bathroom </p>
        <p>-Heat and hot water included!</p>
        <p>
          <br />
        </p>
        <p>--Laundry room</p>
        <p>
          -On-site parking available at an additional fee (private garage spaces
          and outdoor spaces available)
        </p>
        <p>
          <br />
        </p>
        <p>
          <br />
        </p>
        <p>
          Only 10 minutes away from the NYC ferry, and public transportation for
          your convenience, as well as fine dining, great shopping, including
          the new Empire outlets and many other attractions.
        </p>
        <p>
          <br />
        </p>
        <p>Pets welcome</p>
        <p>Guarantors accepted</p>
        <p>
          We have several options in the complex - Reach out today to schedule a
          viewing!
        </p>
      </div>
      <h3
        className="hover:underline  mt-4 flex w-fit cursor-pointer items-center text-sm text-descLightCol"
        onClick={show}
      >
        Read More{" "}
        <span className="ml-2 text-base">
          {showMore ? <FiChevronUp /> : <FiChevronDown />}
        </span>
      </h3>
    </div>
  );
};
