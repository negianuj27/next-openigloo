import Image from "next/image";
import React from "react";
import { DefaultButton, Divider } from "../../../common/index";

type Props = {
  item: {
    imgUrl: string;
    Landmark: string;
    Street: String;
    Locality: String;
    bathroomCount: number;
    bedroomCount: number;
    listBy: String;
    price: number;
    daysPosted: number;
  };
};

const InfoCard = ({ item }: Props) => {
  return (
    <>
      <div className="relative flex flex-col overflow-hidden rounded-lg bg-white shadow-md border border-borderColorLight w-full text-dark-700 mt-4  sm:w-3/5">
        <div className="absolute right-2 top-2 z-10 flex flex-row space-x-2 text-xs">
          <div className="flex justify-center items-center">
            <div className="inline-flex cursor-pointer items-center rounded-full bg-blue-light p-2 ml-2">
              <Image
                src={require("../../../../assets/images/heart.svg")}
                alt="wishlist"
              />
            </div>
          </div>
        </div>
        <div className="max-h-40 overflow-hidden">
          <img src={item?.imgUrl} alt={item?.Landmark} />
        </div>
        <div className="w-full p-2 h-64">
          <div className="truncate">
            <h3 className="text-gray-500 text-md mt-1">{item?.Landmark}</h3>
            <div className="flex justify-between items-center  my-1">
              <div className="font-semibold">
                <h2>
                  {item?.Street}, {item?.Locality}
                </h2>
              </div>
            </div>
          </div>
          <Divider />

          <div className="flex flex-col justify-between items-start h-36">
            <h3 className="text-gray-200 text-md mt-1">No reviews yet</h3>
            <div className="font-thin flex flex-col lg:flex-row justify-start items-start lg:items-center w-full pt-1 lg:space-x-2">
              <div>
                <DefaultButton
                  text="Open violations: 0"
                  onClick={() => console.log("share")}
                  bgColor="bg-blue-light"
                  textColor="text-primary"
                  paddingHorizontal="px-3"
                  paddingVertical="py-0"
                  borderColor="border-primary"
                  textSize="text-xs"
                />
              </div>
              <div className="mt-1 lg:mt-0">
                <DefaultButton
                  text="1 year Bedbug History: No"
                  onClick={() => console.log("share")}
                  bgColor="bg-blue-light"
                  textColor="text-primary"
                  paddingHorizontal="px-3"
                  paddingVertical="py-0"
                  borderColor="border-primary"
                  textSize="text-xs"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default InfoCard;
