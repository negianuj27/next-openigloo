import React from "react";
import InfoCard from "./InfoCard";
import { SimilarListingData } from "../../../../config/mockData/SimilarListings";

type Props = {
};

export const AboutThisBuilding = (props : Props) => {
  return (
    <div className="flex flex-col justify-center items-start px-2 sm:px-0 mb-10 mt-4">
      <h1 className="text-xl font-semibold text-headingCol">
        About this building:
      </h1>
      <InfoCard item={SimilarListingData[0]} />
    </div>
  );
};
