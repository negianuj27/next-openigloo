import React from "react";
import Image from "next/image";

type Props = {};

export const Amenities = (props: Props) => {
  return (
    <div className="flex flex-col justify-center items-start px-2 sm:px-0 my-10">
      <h1 className="text-xl pb-4 font-semibold text-headingCol">Amenities</h1>
      <div>
        <h3 className="py-4 text-xl font-semibold text-headingCol">
          Building amenities
        </h3>
        <div className="flex w-full flex-row flex-wrap sm:w-4/5">
          <div className="mb-3 py-3 px-2 flex w-48 items-center text-base">
            <div className="w-8 h-8 mr-2">
              <Image
                src={require("../../../../assets/images/laundry-room.svg")}
                alt="Laundry-room"
              />
            </div>
            <h2>Laundry Room</h2>
          </div>
          <div className="mb-3 py-3 px-2 flex w-48 items-center text-base">
            <div className="w-8 h-8 mr-2">
              <Image
                src={require("../../../../assets/images/outdoor-space.svg")}
                alt="Outdoor-space"
              />
            </div>
            <h2>Outdoor Space</h2>
          </div>
          <div className="mb-3 py-3 px-2 flex w-48 items-center text-base">
            <div className="w-8 h-8 mr-2">
              <Image
                src={require("../../../../assets/images/parking.svg")}
                alt="Parking"
              />
            </div>
            <h2>Parking</h2>
          </div>
        </div>
      </div>
      <div>
        <h3 className="py-4 text-xl font-semibold text-headingCol">
          Unit amenities
        </h3>
        <div className="flex w-full flex-row flex-wrap sm:w-4/5">
          <div className="mb-3 py-3 px-2 flex w-48 items-center text-base">
            <div className="w-8 h-8 mr-2">
              <Image
                src={require("../../../../assets/images/pets.svg")}
                alt="Pets-allowed"
              />
            </div>
            <h2>Pets Allowed</h2>
          </div>
        </div>
      </div>
    </div>
  );
};
