import { DefaultButton } from "../../../common/index";
import React, { useEffect, useState } from "react";
import useDeviceSize from "../../../../hooks/DeviceSize";
import { BiArrowToTop } from "react-icons/bi";

type Props = {};

export const BackToTop = (props: Props) => {
  const [height] = useDeviceSize();
  const [showBackToTop, setShowBackToTop] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", (event) => {
      if (window.scrollY >= document.documentElement.clientHeight) {
        setShowBackToTop(true);
      } else {
        setShowBackToTop(false);
      }
    });
  });

  return (
    <>
      {showBackToTop ? (
        <div className="sm:sticky mb-5 bottom-8 right-4 z-[45] ml-auto flex justify-end items-center space-x-1   py-2 px-3 text-sm text-white sm:bottom-10 sm:right-0">
          <DefaultButton
            text={
              <div className="flex justify-between items-center">
                <span className="text-2xl">
                  <BiArrowToTop />
                </span>
                <span>Back to Top</span>
              </div>
            }
          />
        </div>
      ) : null}
    </>
  );
};
