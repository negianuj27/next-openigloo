import React from "react";

type Props = {
  data: ProductData.Data
};

export const ListingHistory = ({data}: Props) => {
  return (
    <div className="flex flex-col justify-center items-start px-2 sm:px-0 my-10">
      <h1 className="text-xl pb-4 font-semibold text-headingCol">
        Listing History
      </h1>
      <div className="mt-6 rounded-xl border border-borderColor w-full ">
        <div className="flex justify-around items-center py-3 px-1">
          <p className="w-1/2 pr-6 text-left md:w-3/5">{data.listingDate}</p>
          <p className="w-1/2 pl-6 font-semibold md:w-2/5">${data.price.toLocaleString()}</p>
        </div>
      </div>
    </div>
  );
};
