import React, { useState } from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import useDeviceSize from "../../../hooks/DeviceSize";
import CarouselFullScreen from "./CarouselFullScreen";
const array = [1, 2, 3, 4, 5, 6, 7, 8];

type Props = {
  data: ProductData.Data;
};

export const CarouselMain = ({ data }: Props) => {
  const [width] = useDeviceSize();
  const [fullScreen, setFullScreen] = useState(false);
  const toggleFullScreen = () => {
    setFullScreen(!fullScreen);
  };
  return (
    <>
      <Carousel
        infiniteLoop
        useKeyboardArrows
        autoPlay
        showThumbs={width > 640 ? true : false}
        showStatus={false}
        interval={10000}
        emulateTouch={true}
        swipeable={true}
        thumbWidth={120}
        className="rounded cursor-pointer"
        showIndicators={false}
        onClickItem={(index, number) => {
          toggleFullScreen();
        }}
      >
        {data.imageArray.map((item, index) => {
          return (
            <div className="flex justify-center items-center" key={index}>
              <img src={item} alt="img" className="bg-cover bg-no-repeat" />
            </div>
          );
        })}
      </Carousel>
      {fullScreen && (
        <CarouselFullScreen
          toggleFullScreen={toggleFullScreen}
          imageArray={data.imageArray}
        />
      )}
    </>
  );
};
