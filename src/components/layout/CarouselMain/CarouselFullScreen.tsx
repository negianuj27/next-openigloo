import { DefaultButton } from "myApp/components/common";
import React from "react";
import { ImCross } from "react-icons/im";
import { Carousel } from "react-responsive-carousel";

const array = [1, 2, 3, 4, 5, 6, 7, 8];
const url =
  "https://www.openigloo.com/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fopenigloo-v2.appspot.com%2Fo%2Fbuildings%252F80dcaa1b-7e53-4239-9a9a-e1c772def897%252Funit%252F2B%252Feda8bd29-8ad8-48be-b757-0e5beaa7e277.%3Falt%3Dmedia%26token%3Df40e0949-98a9-4128-be85-f3fc43ad7a0c&w=3840&q=75";

type Props = {
  toggleFullScreen: Function;
  imageArray: string[];
};

function CarouselFullScreen({ toggleFullScreen, imageArray }: Props) {
  return (
    <div className="fixed bg-black bg-opacity-90 top-0 left-0 right-0 py-10 text-gray-200  items-center flex flex-col justify-start  z-[51] h-screen w-screen">
      <span
        className="absolute cursor-pointer top-2 lg:top-10 right-5 lg:right-10 text-xl"
        onClick={() => toggleFullScreen()}
      >
        <ImCross />
      </span>
      <div className="w-full px-1 md:px-0 sm:w-fit items-center justify-between pb-4 sm:flex sm:justify-between false">
        <div className="flex justify-between  items-center ">
          <h1 className="text-sm md:text-base ">
            27 Arlo Road #2B, Staten Island, NY 10301
          </h1>
          <p className="flex px-3 pl-5 text-xl font-semibold text-light-0 ">
            • <span>$1775</span> •
          </p>
          <div className="hidden px-3 text-base  sm:flex ">1bd,1ba</div>
        </div>
        <div className="flex items-center justify-center mt-5 md:mt-0 sm: space-x-2">
          <DefaultButton
            text="Schedule a viewing"
            width="w-44"
            paddingHorizontal="px-1"
            borderColor="border-primary"
            onClick={() => console.log("")}
          />

          <DefaultButton
            bgColor="bg-blue-light"
            width="w-44"
            textColor="text-primary"
            borderColor="textColor"
            text="Apply Now"
            onClick={() => console.log("")}
          />
        </div>
      </div>
      <Carousel
        infiniteLoop
        useKeyboardArrows
        autoPlay
        showThumbs={true}
        showStatus={false}
        interval={10000}
        emulateTouch={true}
        swipeable={true}
        thumbWidth={120}
        className="rounded cursor-pointer w-full md:w-2/4"
        showIndicators={false}
        onClickItem={(index, number) => {
          console.log("clickChange", index, number);
        }}
      >
        {imageArray?.map((item, index) => {
          return (
            <div key={index}>
              <img src={item} alt="img" className="bg-cover bg-no-repeat" />
            </div>
          );
        })}
      </Carousel>
    </div>
  );
}

export default CarouselFullScreen;
