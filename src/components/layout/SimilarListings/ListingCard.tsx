import Image from "next/image";
import React from "react";
import { DefaultButton, Divider } from "../../common";

type Props = {
  item: {
    imgUrl: string;
    Landmark: string;
    Street: String;
    Locality: String;
    bathroomCount: number;
    bedroomCount: number;
    listBy: String;
    price: number;
    daysPosted: number;
  };
};

const ListingCard = ({ item }: Props) => {
  return (
    <>
      <div className="relative flex flex-col overflow-hidden rounded-lg bg-white shadow-md border border-borderColorLight w-full text-dark-700">
        <div className="absolute right-2 top-2 z-10 flex flex-row space-x-2 text-xs">
          <div className="flex justify-center items-center">
            <DefaultButton
              text="Verified"
              onClick={() => console.log()}
              textSize="text-xs"
              bgColor="bg-primaryDark"
              paddingHorizontal="px-2"
              paddingVertical="py-1"
            />
            <div className="inline-flex cursor-pointer items-center rounded-full bg-blue-light p-2 ml-2">
              <Image
                src={require("../../../assets/images/heart.svg")}
                alt="wishlist"
              />
            </div>
          </div>
        </div>
        <div className="max-h-40 overflow-hidden">
          <img src={item?.imgUrl} alt={item?.Landmark} />
        </div>
        <div className="w-full p-2">
          <div className="truncate">
            <h3 className="text-gray-500 text-sm mt-1">{item?.Landmark}</h3>
            <div className="flex justify-between items-center  my-1">
              <div className="font-semibold">
                <h2>{item?.Street}</h2>
                <h2>{item?.Locality}</h2>
              </div>
              <p className="text-xs font-thin text-dark-700">
                {item.daysPosted} days ago
              </p>
            </div>
            <div className="flex flex-row items-center justify-start w-full space-x-1 text-sm mt-5">
              <div className="flex justify-start items-center mr-2">
                <Image
                  src={require("../../../assets/images/bedroom.svg")}
                  alt="bedroom"
                />
                <p className="ml-1">{item?.bedroomCount}</p>
              </div>
              <div className="flex justify-start items-center mr-2">
                <Image
                  src={require("../../../assets/images/bathroom.svg")}
                  alt="bathroom"
                />
                <p className="ml-1">{item?.bathroomCount}</p>
              </div>
            </div>
          </div>
          <Divider />
          <div>
            <h2 className="my-2 text-xl">${item?.price}</h2>
            <p className="text-xs font-thin">Listed by {item?.listBy}</p>
          </div>

          <div className="font-thin flex flex-row justify-end items-center w-full px-2 py-4 space-x-2">
            <DefaultButton
              text={
                <div className="flex justify-between items-center">
                  <Image
                    src={require("../../../assets/images/share.svg")}
                    alt="share"
                  />
                  <span className="text-black  ml-2">Share</span>
                </div>
              }
              onClick={() => console.log("share")}
              bgColor="text-white"
              textColor="text-black"
              paddingHorizontal="px-2"
            />
            <DefaultButton
              text="I'm Interested"
              onClick={() => console.log("")}
              paddingHorizontal="px-10"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default ListingCard;
