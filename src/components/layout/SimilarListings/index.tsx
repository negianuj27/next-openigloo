import React, { useEffect, useState } from "react";
import ListingCard from "./ListingCard";
import {
  getSimilarListingData,
  similarListingData,
} from "../../../services/similarListing";
import { API } from "../../../services/utils/defines";

type Props = {};

const SimilarListings = (props: Props) => {
  const [similarListingData, setSimilarListingData] =
    useState<similarListingData.Response>([]);
  const handleGetData = async () => {
    try {
      const similarListingDataResponse = await getSimilarListingData(
        API.SimilarListings
      );
      if (similarListingDataResponse) {
        setSimilarListingData(similarListingDataResponse);
      }
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    handleGetData();
  }, []);
  return (
    <div className="flex flex-col justify-between items-start my-10 pb-12 sm:pb-0 px-4 sm:px-0">
      <h1 className="text-xl font-semibold text-headingCol">
        Similar Listings
      </h1>
      <div className="mt-6 grid grid-cols-1 gap-4 md:grid-cols-3 ">
        {similarListingData.map((item, i) => {
          return <ListingCard item={item} key={i} />;
        })}
      </div>
    </div>
  );
};

export default SimilarListings;
