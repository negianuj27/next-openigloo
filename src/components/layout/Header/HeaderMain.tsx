import React from "react";
import Image from "next/image";
import { OPENIGLOO_LOGO } from "../../../services/utils/defines";

type Props = {};

const HeaderMain = (props: Props) => {
  return (
    <div className="w-full  sticky py-1 bg-headerColor  border border-borderColor h-max ">
      <div className="max-w-screen-lg flex justify-between items-center mx-auto">
        <div className="h-full w-28">
          <img src={OPENIGLOO_LOGO} className="max-w-full" />
        </div>
        <div className="h-10 bg-white border leading-tight border-borderColor  text-sm text-black shadow-sm w-9/12 md:w-3/5 rounded-3xl flex justify-center items-center">
          <div className="ml-2">
            <Image
              src={require("../../../assets/images/searchIcon.svg")}
              alt="search-icon"
            />
          </div>
          <input
            className="inline-block w-11/12 bg-transparent px-1 pr-4 text-dark-900 focus:outline-none"
            placeholder="Search for an address"
          />
        </div>
        <div className="flex justify-between items-center space-x-2 rounded-full p-1 px-2 bg-white shadow-sm">
          <Image
            src={require("../../../assets/images/menuIcon.svg")}
            alt="menu-icon"
          />
          <Image
            src={require("../../../assets/images/profile.svg")}
            alt="profile-icon"
          />
        </div>
      </div>
    </div>
  );
};

export default HeaderMain;
