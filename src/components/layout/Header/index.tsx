import React from "react";
import HeaderMain from "./HeaderMain";
import HeaderResponsive from "./HeaderResponsive";
import useDeviceSize from "../../../hooks/DeviceSize";

type Props = {};

const Header = (props: Props) => {
  const [width] = useDeviceSize();
  return <>{width > 1024 ? <HeaderMain /> : <HeaderResponsive />}</>;
};

export default Header;
