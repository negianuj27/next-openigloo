import React from "react";
import Image from "next/image";

type Props = {};

const HeaderResponsive = (props: Props) => {
  return (
    <div className="sticky top-0 z-50 w-full p-2 bg-headerColor  border border-borderColor h-max ">
      <div className="max-w-screen-lg flex justify-between items-center mx-auto">
        <div className="rounded-full p-1  bg-white shadow-sm w-max  flex items-center justify-center">
          <Image
            src={require("../../../assets/images/backBtn.svg")}
            className="max-w-full"
            alt="back-btn"
          />
        </div>
        <div className="h-10 bg-white border leading-tight border-borderColor  text-sm text-black shadow-sm w-9/12 md:w-3/5 rounded-3xl flex justify-center items-center">
          <div className="ml-2">
            <Image
              src={require("../../../assets/images/searchIcon.svg")}
              alt="search-icon"
            />
          </div>
          <input
            className="inline-block w-11/12 bg-transparent px-1 pr-4 text-dark-900 focus:outline-none"
            placeholder="Search for an address"
          />
        </div>
        <div className="h-10 w-10 flex justify-center items-center">
          <Image
            src={require("../../../assets/images/menuIcon.svg")}
            alt="menu-icon"
          />
        </div>
      </div>
    </div>
  );
};

export default HeaderResponsive;
