import React from "react";

type Props = {
  text: String | JSX.Element;
  onClick?: (event: React.MouseEvent<HTMLElement>) => void;
  bgColor?: String;
  textColor?: String;
  borderColor?: String;
  textSize?: String;
  paddingHorizontal?: String;
  paddingVertical?: String;
  width?: String;
};

export const DefaultButton = ({
  text,
  onClick,
  bgColor,
  textColor,
  borderColor,
  textSize,
  paddingHorizontal,
  paddingVertical,
  width,
}: Props) => {
  const btnStyle = `${bgColor} ${textColor} ${paddingHorizontal} ${paddingVertical} rounded-3xl font-semiBold ${textSize} border ${borderColor} ${width}`;
  return (
    <button className={btnStyle} onClick={onClick}>
      {text}
    </button>
  );
};

DefaultButton.defaultProps = {
  text: "",
  width: "",
  bgColor: "bg-primary",
  textColor: "text-white",
  borderColor: "border-borderColor",
  textSize: "text-sm",
  paddingHorizontal: "px-6",
  paddingVertical: "py-2",
};
