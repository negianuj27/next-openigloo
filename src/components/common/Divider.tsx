import React from "react";

type Props = {};

export const Divider = (props: Props) => {
  return <div className="my-4 h-px bg-gray-200 w-full mx-auto" />;
};
