namespace ProductData {
  export interface Data {
    Landmark: string;
    Locality: string;
    Street: string;
    zipcode: string;
    bathroomCount: number;
    bedbug: boolean;
    bedroomCount: number;
    daysPosted: number;
    imageArray: string[];
    imagePrimary: string;
    listBy: string;
    listingDate: string;
    openViolations: number;
    price: number;
    verified: boolean;
  }
}
