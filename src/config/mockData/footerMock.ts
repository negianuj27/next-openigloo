export const FooterItem = [
  { heading: "About", subItems: ["Login"] },
  { heading: "Support", subItems: ["Contact Us", "FAQs"] },
  {
    heading: "Community",
    subItems: [
      "Community Guildelines",
      "Our Blogs",
      "NYC Buildings",
      "Rent Report",
    ],
  },
  {
    heading: "For Landlords",
    subItems: [
      "Submit Your Listings",
      "Listings Quality Policy",
      "For Owners & Managers",
    ],
  },
];
