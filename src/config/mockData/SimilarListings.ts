export const SimilarListingData = [
  {
    imgUrl:
      "https://www.openigloo.com/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fopenigloo-v2.appspot.com%2Fo%2Fbuildings%252F62f724ee-7149-4f1b-b84f-66cd1593cdd5%252Funit%252F2AA%252F6fe42862-b98c-407f-adf6-a878f9f2c2a0.heic%3Falt%3Dmedia%26token%3Dd8ea244a-2351-461d-a8a4-ed54643e8e84&w=2048&q=75",
    Landmark: "Grymes Hill",
    Street: "128 Arlo Road #2aa",
    Locality: "Staten Island, NY",
    bathroomCount: 1,
    bedroomCount: 1,
    listBy: "Allia",
    price: 1795,
    daysPosted: 2,
  },
  {
    imgUrl:
      "https://www.openigloo.com/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fopenigloo-v2.appspot.com%2Fo%2Fbuildings%252F7f326aff-9065-4797-b7e7-e74f485faffc%252Funit%252F2B%252F03d9007f-b1ce-4ceb-92c7-09721fd9a063.%3Falt%3Dmedia%26token%3D4ec8458e-e043-4306-816d-d58be00bffde&w=2048&q=75",
    Landmark: "Grymes Hill",
    Street: "33 Stratford Avenue #2b",
    Locality: "Staten Island, NY",
    bathroomCount: 1,
    bedroomCount: 1,
    listBy: "Allia",
    price: 1875,
    daysPosted: 5,
  },
  {
    imgUrl:
      "https://www.openigloo.com/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fopenigloo-v2.appspot.com%2Fo%2Fbuildings%252F59f9a96c-6586-4077-88b2-e3b1117a092b%252Funit%252F1B%252Fd37b4b1e-2f74-4c6d-b53a-22ecaa9dbdae.%3Falt%3Dmedia%26token%3D7691c418-4260-4bf6-ab12-9359a2de542a&w=2048&q=75",
    Landmark: "Grymes Hill",
    Street: "80 Arlo Road #1b",
    Locality: "Staten Island, NY",
    bathroomCount: 1,
    bedroomCount: 1,
    listBy: "Allia",
    price: 1650,
    daysPosted: 5,
  },
];
