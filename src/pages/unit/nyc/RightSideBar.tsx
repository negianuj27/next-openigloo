import React from "react";
import { DefaultButton } from "../../../components/common";

type Props = {
  data: ProductData.Data;
};

const RightSideBar = ({ data }: Props) => {
  return (
    <div className="px-4 sm:px-0 sticky top-20 z-[49]">
      <div className="flex items-center justify-between">
        <div className="text-base font-semibold text-mid-400">
          {data?.Landmark}
        </div>
        <div className="inline-flex items-center  py-1 px-4 text-xs font-semibold rounded-xl text-white border-blue-25 border ml-4 bg-black">
          <span className="">{data.verified ? "Verified" : "Unverified"}</span>
        </div>
      </div>
      <div className="mt-4 pr-4 text-lg font-semibold text-dark-900">
        {data?.Street}, {data?.Locality} {data?.zipcode}
      </div>
      <div className="relative">
        <div className="">
          <div className="mt-3 flex justify-between text-dark-900">
            <div className="w-2/4 text-2xl font-semibold">${data?.price}</div>
            <div className="flex w-2/4 items-center justify-end"></div>
          </div>
          <div className="my-8">
            <div className="rounded-lg border border-blue-light p-4 sm:p-6">
              <div className="flex">
                <div className="w-1/3 pr-3">
                  <div className="pt-1 text-base font-semibold text-dark-600">
                    {data?.bedroomCount} bed
                  </div>
                </div>
                <div className="w-1/3 pr-3">
                  <div className="pt-1 text-base font-semibold text-dark-600">
                    {data?.bathroomCount} bath
                  </div>
                </div>
              </div>
              <div className="h-px bg-gray-200 my-5"></div>
              <div className="flex">
                <div className="w-1/3 pr-3">
                  <div className="text-sm text-mid-400">Available:</div>
                  <div className="pt-1 text-base font-semibold text-dark-600">
                    {data?.listingDate}
                  </div>
                </div>
                <div className="w-1/3 pr-3">
                  <div className="text-sm text-mid-400">On market:</div>
                  <div className="pt-1 text-base font-semibold text-dark-600">
                    {data?.daysPosted} days
                  </div>
                </div>
                <div className="w-1/3">
                  <div className="text-sm text-mid-400">Pet policy:</div>
                  <div className="pt-1 text-base font-semibold text-dark-600">
                    Pets allowed
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="rounded-lg border border-blue-light bg-white p-4 shadow-md sm:p-6">
            <div className="flex flex-wrap">
              <div className="m-0 pr-3 text-left md:w-2/5">
                <div className="text-sm text-mid-400">Posted By:</div>
                <div className="pt-1 text-base font-semibold text-dark-600">
                  {data?.listBy}
                </div>
              </div>
              <div className="m-auto flex flex-col sm:flex-row sm:block text-right gap-x-4 md:w-3/5">
                <div>
                  <DefaultButton
                    text="Schedule a viewing"
                    width="w-44"
                    borderColor="border-primary"
                  />
                </div>

                <div className="mt-2">
                  <DefaultButton
                    bgColor="bg-blue-light"
                    width="w-44"
                    textColor="text-primary"
                    borderColor="textColor"
                    text="Apply Now"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RightSideBar;
