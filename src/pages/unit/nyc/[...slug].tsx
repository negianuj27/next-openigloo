import SimilarListings from "../../../components/layout/SimilarListings";
import { GetServerSideProps } from "next";
import React, { useEffect, useState } from "react";
import LeftSideBar from "./LeftSideBar";
import RightSideBar from "./RightSideBar";
import { Divider } from "../../../components/common";
import { BackToTop } from "../../../components/layout/nyc";
import { getHomeDetail, homeDetailData } from "myApp/services/homeDetail";
import { API } from "myApp/services/utils/defines";

type Props = {
  slug: string;
  data: ProductData.Data;
};

const RentailDetail = ({ slug, data }: Props) => {
  return (
    <div className="flex-1">
      <div className="mx-auto my-8 max-w-screen-lg justify-center text-gray-700 sm:px-4 mb-10 lg:mb-0">
        <div className="sm:flex sm:space-x-5">
          <LeftSideBar data={data} />
          <div className="mb-11 items-start hidden sm:block sm:w-5/12 px-2 sm:px-0">
            <RightSideBar data={data} />
          </div>
        </div>
        <Divider />
        <SimilarListings />
        <BackToTop />
      </div>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async ({
  params,
}: any) => {
  const { slug } = params;
  const homeDetail = await getHomeDetail(API.detail);

  return {
    props: {
      slug,
      data: homeDetail,
    },
  };
};

export default RentailDetail;
