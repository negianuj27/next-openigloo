import { DefaultButton, Divider } from "../../../components/common/index";
import Image from "next/image";
import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { CarouselMain } from "../../../components/layout/CarouselMain";
import {
  AboutThisBuilding,
  Description,
  Amenities,
  ListingHistory,
} from "../../../components/layout/nyc";
import useDeviceSize from "../../../hooks/DeviceSize";
import RightSideBar from "./RightSideBar";

type Props = {
  data: ProductData.Data;
};

const LeftSideBar = ({ data }: Props) => {
  const [width] = useDeviceSize();
  return (
    <div className="sm:w-7/12 sm:px-0">
      <div className="flex justify-end items-center mb-6">
        <DefaultButton
          text={
            <div className="flex justify-between items-center">
              <Image
                src={require("../../../assets/images/share.svg")}
                alt="share"
              />
              <span className="text-black  ml-2">Share</span>
            </div>
          }
          onClick={() => console.log("share")}
          bgColor="text-white"
          textColor="text-black"
        />
        <div className="inline-flex cursor-pointer items-center rounded-full bg-blue-light p-3 ml-2">
          <Image
            src={require("../../../assets/images/heart.svg")}
            alt="wishlist"
          />
        </div>
      </div>
      <CarouselMain data={data} />
      {width < 640 && (
        <div className="mt-4">
          <RightSideBar data={data} />
        </div>
      )}
      <AboutThisBuilding />
      <Divider />
      <Description />
      <Divider />
      <Amenities />
      <Divider />
      <ListingHistory data={data} />
    </div>
  );
};

export default LeftSideBar;
