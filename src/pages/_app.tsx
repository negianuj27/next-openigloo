import "../styles/globals.css";
import React from "react";
import type { AppProps } from "next/app";
import { Provider } from "react-redux";
import { store } from "../store";
import { QueryClient, QueryClientProvider } from "react-query";
import Header from "../components/layout/Header";
import { Nunito } from "next/font/google";
import Footer from "../components/layout/Footer";

const nunito = Nunito({ subsets: ["latin"] });

const queryClient = new QueryClient();

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <QueryClientProvider client={queryClient}>
        <main className={nunito.className}>
          <Header />
          <Component {...pageProps} />
          <Footer />
        </main>
      </QueryClientProvider>
    </Provider>
  );
}
