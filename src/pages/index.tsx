import Head from "next/head";
import Image from "next/image";
import styles from "myApp/styles/Home.module.css";
import { DefaultButton } from "myApp/components/common";
import React from "react";
import { useRouter } from "next/router";

export default function Home() {
  const router = useRouter();
  return (
    <>
      <Head>
        <title>Home</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="mx-auto flex justify-center items-center h-screen">
        <DefaultButton
          text={"Go !!"}
          onClick={() =>
            router.push(
              `/unit/nyc/5158924-5005930590-2b/staten-island-clifton-27-arlo-ro…`
            )
          }
        />
      </main>
    </>
  );
}
