/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",

    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
    colors: {
      black: "#000000",
      headerColor: "#fafbfc",
      borderColor: "#dfe1e5",
      borderColorLight: "#DFE1E6",
      footerBg: "#f4f5f7",
      white: "#fff",
      primary: "#6664a1",
      primaryDark: "#050035",
      mainDark: "#070052",
      headingCol: "#374151",
      descCol: "#253858",
      descLightCol: "#6764A1",
      "dark-700": "#253858",
      "blue-gray": "#424067",
      "blue-light": "#EAEAFF",
      "gray-200": "#E5E7EB",
      "gray-400": "#6B7280",
      "gray-500": "#505F79",
    },
    fontSize: {
      xs: "0.75rem",
      sm: "0.875rem",
      base: "1rem",
      lg: "1.125rem",
      xl: "1.25rem",
      "2xl": "1.5rem",
      "3xl": "1.875rem",
      "4xl": "2.25rem",
      "5xl": "3rem",
      "6xl": "4rem",
    },
  },
  plugins: [require("@tailwindcss/line-clamp")],
};
